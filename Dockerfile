FROM node:8.10.0-alpine

COPY . /app
WORKDIR /app

RUN set -x \
    && npm install

ENTRYPOINT [ ]
CMD [ "npm","start" ]